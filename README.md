# tatale

tatale: Tools for Assessment with Transformation and Aggregation using simple Logic and Expertise

## Description
In short, tatale is a simple multi-criteria assessment tool based on expertise.
tatale is an indicator assessment tool constructed from many variables. This construction is based on two principles: 

1. Expertise on variation ranges and key relationships between variables, 
2. Normalization and aggregation of variables for calculating summary scores. These summary scores that can be considered as latent variables will be used as indicators to help address issues that challenge today's research on agro-ecosystems farms.

tatale is available as an R package that allows easy calculation of so-called "complex indicators." It also responds to some extent to the problem of selecting variables for the development of indicators when the links and interactions are numerous.

To get you started, a quick guide is available [here](https://gitlab.cirad.fr/selmet/tatale/-/blob/d8b8d871daead868bbee2b3934c555e0c0e720ed/doc/manual_tatale_en.pdf)

## Installation in RStudio

So as to install the package tatale directly from the remote repository gitlab, you need to ensure that you have Git software installed on your system. More details [here](https://support.posit.co/hc/en-us/articles/200532077)

Then you use install_git from the package [remotes.](https://CRAN.R-project.org/package=remotes) 

```R
install.packages("remotes")
library(remotes)
```

for the current developing version:
```R
remotes::install_git('https://gitlab.cirad.fr/selmet/tatale')
```
or for a specific version:
```R
remotes::install_git('https://gitlab.cirad.fr/selmet/tatale', ref = "0.1.0")
```

## Support
please send a message to the maintainer: Samir Messad <samir.messad@cirad.fr>

## Authors
Simon Taugourdeau <simon.taugourdeau@cirad.fr>, Samir Messad <samir.messad@cirad.fr>

## License
[GPL (>= 3)](https://www.gnu.org/licenses/gpl-3.0.html)

## Project status
The package is currently under development (organization of functions and help pages).  A version 1.0 is in preparation and will be released very soon.
