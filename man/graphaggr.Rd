\name{graphaggr}
\alias{graphaggr}
\title{graphaggr}
\description{
produce a graphical representation of the aggregation (aggregation tree) described in the table tabaggr which contains the aggregation for the construction of the scores.
}
\usage{
graphaggr(tabaggr, orientation = "LR", space = 1)
}
\arguments{
  \item{tabaggr}{
a data frame provides by the user which describes parameters of the aggregation. For more details, see the help of \code{\link{aggr}} function.
}
  \item{orientation}{
orientation of the aggregation tree. Two options are possible: (i) default option is "LR", the tree is oriented from the left (variables from the transformation) to the right (scores obtained after aggregation). (ii) "TD", the tree is oriented from the top to the bottom.
}
  \item{space}{
a graphic parameter to control the distance between labels of variables/scores on the graphic. Default value is 1.
}
}

\value{
a graphical representation  of an aggregation tree.
}

\seealso{
\code{\link{aggr}}
}
\examples{
ON <- factor(c("A1","A1","A2","A2","A2","A3","A3"))
IN <- factor(c("V1","V2","V3","V4","V5","A1","A2"))
method <- c("MEAN","MEAN","MIN","MIN","MIN","MAX","MAX")
weight <- as.numeric(c(2,1,1,1,1,1,1))
tabaggrex <- data.frame(ON, IN, method, weight)

graphaggr(tabaggrex, orientation="LR", space=1)
}
